const onLoad = () => {

    // Creo la tabla 4x4 

    // los turnos son maquina, usuario, maquina 

    // Al llegar a la utlima fila, el sistema debe decir 

    // El ejercicio consiste en determinar la posición o posiciones en las que 
    // la máquina resultará vencedora en función de cómo se hayan rellenado el resto de filas,
    //  indicando la celda a rellenar o en caso contrario indicando empate o derrota.

    let main = document.getElementsByTagName("main")[0]
    let tabla = document.createElement("table")
    tabla.className = "tablero"
    let tblBody = document.createElement("tbody");

    
    for (var f = 1; f <= 4; f++) {
        
        var fila = document.createElement("tr");

        var turnos 
        for (var c = 1; c <= 4; c++) {

            var celda = document.createElement("td");
            var img = document.createElement("img")
            img.style.width = "100px";

        
            

            if(c % 2 == 0){
                // Maquina
                img.src = `/images/maquina.png`

            } else {
                
                // Jugador
                img.src = `/images/jugador.png`
            }
            
            var num =  Math.round(Math.random())

            celda.appendChild(img);
            celda.style.border = "2px solid" 
            fila.appendChild(celda);
        }

        
        tblBody.appendChild(fila);
    }

    tabla.appendChild(tblBody)
    main.appendChild(tabla)
}